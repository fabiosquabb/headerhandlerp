package com.example.demo;

//import com.example.demo.pojo.;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class HeaderHandlerApplication {

	public static void main(String[] args) {
		SpringApplication.run(HeaderHandlerApplication.class, args);
	}

}
