package com.example.demo.controllers;

//import com.example.demo.pojo.;
//import com.example.demo.services.;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.mapping.Map;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@AllArgsConstructor
@RestController
@Slf4j

//@RequestMapping("/users") // http://localhost:8080/users
public class HeaderHandlerController {

    /*@PostMapping(path = "/myrequestheaders", produces = {MediaType.APPLICATION_JSON_VALUE} )
    public ResponseEntity<Map<String, String>> getMyRequestHeaders(
            @RequestHeader(value="Accept") String acceptHeader,
            @RequestHeader(value="Authorization") String authorizationHeader
    )
    {
        Map<String, String> returnValue = new HashMap<>();
        ((HashMap<?, ?>) returnValue).put("Accept", acceptHeader);
        returnValue.put("Authorization", authorizationHeader);

        return ResponseEntity.status(HttpStatus.OK).body(returnValue);
    }*/

    @PostMapping("/qualcosa")
    public ResponseEntity<?> testGetHeaders(@RequestHeader HttpHeaders headers){
        log.info("");

        return ResponseEntity.status(HttpStatus.OK).body("BOH");
    }
}

