package com.example.demo.controllers;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.mapping.Map;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@AllArgsConstructor
@RestController
@Slf4j

public class FormatCheckController {

    RestTemplate restTemplate = new RestTemplate();
    String objResourceUrl
            = "http://localhost:8080/qualcosa";
    ResponseEntity<String> response
            = restTemplate.getForEntity(objResourceUrl + "/1", String.class);

}
