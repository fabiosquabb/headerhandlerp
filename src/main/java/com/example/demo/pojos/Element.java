package com.example.demo.pojos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class Element {

    private String elementId;
    public String elementName;

    public Element(String name, String elementId) {
        this.elementName = name;
        this.elementId = elementId;
    }
    public Element(String name) {
        this.elementName = name;
    }

}
